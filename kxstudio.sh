#!/bin/bash
#
# Install required dependencies if needed
sudo apt-get install apt-transport-https software-properties-common wget &&
#
# Download package file
wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos_9.5.1~kxstudio3_all.deb &&
#
# Install it
sudo dpkg -i kxstudio-repos_9.5.1~kxstudio3_all.deb &&
#
#Install required dependencies if needed
sudo apt-get install libglibmm-2.4-1v5 &&

# Download package file
wget https://launchpad.net/~kxstudio-debian/+archive/kxstudio/+files/kxstudio-repos-gcc5_9.5.1~kxstudio3_all.deb &&
#
# Install it
sudo dpkg -i kxstudio-repos-gcc5_9.5.1~kxstudio3_all.deb
#
#Update Sytem
sudo apt-get update && sudo apt-get upgrade
#
sudo rm kxstudio-repos*
